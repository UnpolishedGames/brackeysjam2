﻿using UnityEngine;

namespace UnpolishedGames
{
    [CreateAssetMenu(fileName = "HairColorAsset", menuName = "Unpolished Games/Character Assets/Hair color Asset")]
    public class HairColorAsset : ScriptableObject
    {
        public Color[] color;
    }
}