﻿using UnityEngine;

namespace UnpolishedGames
{
    [CreateAssetMenu(fileName = "ShoeColorAsset", menuName = "Unpolished Games/Character Assets/Shoe color Asset")]
    public class ShoeColorAsset : ScriptableObject
    {
        public Color[] color;
    }
}