﻿using UnityEngine;

namespace UnpolishedGames
{
    [CreateAssetMenu(fileName = "ShirtColorAsset", menuName = "Unpolished Games/Character Assets/Shirt color Asset")]
    public class ShirtColorAsset : ScriptableObject
    {
        public Color[] color;
    }
}