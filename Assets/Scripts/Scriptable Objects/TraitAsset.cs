﻿using UnityEngine;

namespace UnpolishedGames
{
    [CreateAssetMenu(fileName = "TraitAsset", menuName = "Unpolished Games/Trait Asset")]
    public class TraitAsset : ScriptableObject
    {
        public Sprite sprite;
        public string Name;
        public int traitValue;
    }
}