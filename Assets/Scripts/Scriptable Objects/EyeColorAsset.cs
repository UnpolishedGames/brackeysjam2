﻿using UnityEngine;

namespace UnpolishedGames
{
    [CreateAssetMenu(fileName = "EyeColorAsset", menuName = "Unpolished Games/Character Assets/Eye color Asset")]
    public class EyeColorAsset : ScriptableObject
    {
        public Color[] color;
    }
}