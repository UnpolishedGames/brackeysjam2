﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace  UnpolishedGames 
{
    [CreateAssetMenu(fileName = "ThoughtIconsAsset", menuName = "Unpolished Games/Thought Icons Asset")]
    public class ThoughtIconsAsset : ScriptableObject {
        public Sprite[] icons;
    }
}

