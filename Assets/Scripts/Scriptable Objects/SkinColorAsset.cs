﻿using UnityEngine;

namespace UnpolishedGames
{
    [CreateAssetMenu(fileName = "SkinColorAsset", menuName = "Unpolished Games/Character Assets/Skin color Asset")]
    public class SkinColorAsset : ScriptableObject
    {
        public Color[] color;
    }
}