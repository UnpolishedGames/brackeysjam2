﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnpolishedGames
{
    public class CharacterAI : MonoBehaviour
    {
        #region Variables
        [SerializeField] private float minX, minY, maxX, maxY;
        [SerializeField] private float minSpeed, maxSpeed;
        [SerializeField] private float directMovePercentage = 20f;
        [SerializeField] private bool isMeetingPartner = false;
        [SerializeField] private bool isMovingToDespawner = false;

        [SerializeField] Transform despawner;

        private Animator animator;

        private float speed;

        [SerializeField] private float timeBetweenMoves;
        private float nextMoveTime;

        [SerializeField] private float timeBetweenBlinks;
        private float nextBlinkTime;

        [SerializeField] private float timeWaitigOnPartner = 5f;
        private float moveToDespawnTime;

        private Vector2 targetPos;

        #endregion

        #region Unity Methods
        // Use this for initialization
        void Start()
        {
            despawner = GameObject.Find("Despawner").transform;

            animator = GetComponent<Animator>();
            speed = Random.Range(minSpeed, maxSpeed);

            SetNewTarget();
            if (Random.Range(0, 100) > directMovePercentage)
            {
                transform.position = targetPos;
            }

            nextBlinkTime = Time.time + Random.Range(timeBetweenBlinks, 2 * timeBetweenBlinks);
        }

        // Update is called once per frame
        void Update()
        {

            if (Time.time >= nextBlinkTime)
            {
                animator.SetTrigger("Blink");
                nextBlinkTime = Time.time + Random.Range(timeBetweenBlinks, 2 * timeBetweenBlinks);
            }

            if (Vector2.Distance(transform.position, targetPos) > 0.4f)
            {
                transform.position = Vector2.MoveTowards(transform.position, targetPos, speed * Time.deltaTime);
                animator.SetBool("IsWalking", true);
            }
            else
            {
                if (isMovingToDespawner)
                {
                    Destroy(this.gameObject);
                    PointScoredEvent pointScoredEvent = new PointScoredEvent();
                    pointScoredEvent.FireEvent();
                }
                animator.SetBool("IsWalking", false);
                if (isMeetingPartner)
                {
                    if (Time.time >= moveToDespawnTime)
                    {
                        MatchMadeInHeavenEvent matchMadeEvent = new MatchMadeInHeavenEvent();
                        matchMadeEvent.FireEvent();
                        isMovingToDespawner = true;
                        setMeetingPoint(despawner.position);
                    }
                }
                else
                {
                    if (Time.time >= nextMoveTime)
                    {
                        SetNewTarget();
                    }
                }
            }
        }
        #endregion

        #region Unpolished Methods
        public void setMeetingPoint(Vector2 meetingPoint)
        {
            this.isMeetingPartner = true;
            targetPos = meetingPoint;
            speed = 4f; // set matching speeds for characters
            moveToDespawnTime = Time.time + timeWaitigOnPartner;
            this.gameObject.GetComponent<CharacterBehaviour>().IsMatched = true;
        }
        private void SetNewTarget()
        {
            float PosX = Random.Range(minX, maxX);
            float PosY = Random.Range(minY, maxY);

            targetPos = new Vector2(PosX, PosY);
            nextMoveTime = Time.time + Random.Range(timeBetweenMoves, 2 * timeBetweenMoves);
        }
        #endregion
    }
}