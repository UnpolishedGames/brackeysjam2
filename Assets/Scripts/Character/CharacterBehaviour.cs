﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnpolishedGames
{
    public class CharacterBehaviour : MonoBehaviour
    {

        #region Variables
        [Header("Character Part renders")]
        [SerializeField] private SpriteRenderer hair;
        [SerializeField] private SpriteRenderer head;
        [SerializeField] private SpriteRenderer eyes;
        [SerializeField] private SpriteRenderer shirt;
        [SerializeField] private SpriteRenderer leftHand;
        [SerializeField] private SpriteRenderer rightHand;
        [SerializeField] private SpriteRenderer leftShoe;
        [SerializeField] private SpriteRenderer rightShoe;
        [SerializeField] private Sprite heartEyesSprite;

        [Header("Character Color Assets")]
        [SerializeField] private HairColorAsset haircolors;
        [SerializeField] private SkinColorAsset skincolors;
        [SerializeField] private EyeColorAsset eyecolors;
        [SerializeField] private ShirtColorAsset shirtcolors;
        [SerializeField] private ShoeColorAsset shoecolors;

        [Header("Character Trait info")]
        [SerializeField] private long uniqueTraitsId;
        [SerializeField] private TraitAsset[] traitInfo;

        [Header("Character thought settings")]
        [SerializeField] private Animator thoughtAnim;
        [SerializeField] private SpriteRenderer thoughtSpriteRenderer;
        [SerializeField] private float timeBetweenThoughts;
        [SerializeField] private float timeThoughtShown;
        private float nextThoughtTime;
        private float thoughtEndTime;

        [Header("Character state info")]
        [SerializeField] private bool isSingle;
        [SerializeField] private bool isMatched;
        [SerializeField] private ParticleSystem loveBoom;

        public long UniqueTraitsId { get => uniqueTraitsId; }
        public bool IsMatched { get => isMatched; set => isMatched = value; }

        private Sprite originalEyesSprite;
        private Color originalEyeColor;
        private ParticleSystem ps;
        #endregion

        #region Unity Methods

        // Use this for initialization
        void Start()
        {
            hair.color = haircolors.color[Random.Range(0, haircolors.color.Length)];
            eyes.color = eyecolors.color[Random.Range(0, eyecolors.color.Length)];
            head.color = leftHand.color = rightHand.color = skincolors.color[Random.Range(0, skincolors.color.Length)];
            shirt.color = shirtcolors.color[Random.Range(0, shirtcolors.color.Length)];
            leftShoe.color = rightShoe.color = shoecolors.color[Random.Range(0, shoecolors.color.Length)];

            originalEyesSprite = eyes.sprite;
            originalEyeColor = eyes.color;

            nextThoughtTime = Time.time + Random.Range(timeBetweenThoughts, 2 * timeBetweenThoughts);
            ps = Instantiate(loveBoom, new Vector3(transform.position.x, transform.position.y), Quaternion.identity);
        }

        public void SetEyes()
        {
            eyes.sprite = heartEyesSprite;
            eyes.color = Color.white;
        }

        public void ResetEyes()
        {
            eyes.sprite = originalEyesSprite;
            eyes.color = originalEyeColor;
        }

        // Update is called once per frame
        void Update()
        {
            if (Time.time >= nextThoughtTime)
            {
                thoughtSpriteRenderer.sprite = traitInfo[Random.Range(0, traitInfo.Length)].sprite;
                thoughtAnim.SetBool("ShowTrait", true);
                thoughtEndTime = Time.time + timeThoughtShown;
                nextThoughtTime = Time.time + Random.Range(timeBetweenThoughts, 2 * timeBetweenThoughts);
            }

            if (Time.time >= thoughtEndTime)
            {
                thoughtAnim.SetBool("ShowTrait", false);
            }
        }
        #endregion

        #region Unpolished Methods
        // public void SetTraitData(TraitData traitData)
        // {
        //     this.traitData = traitData;
        // }

        public void SetTraitInfo(TraitAsset[] traitInfo)
        {
            this.traitInfo = traitInfo;
            for (int i = 0; i < this.traitInfo.Length; i++)
            {
                this.uniqueTraitsId += this.traitInfo[i].traitValue;
            }
        }

        // public TraitData GetTraitData()
        // {
        //     return this.traitData;
        // }

        public void SetSingle(bool isSingle)
        {
            this.isSingle = isSingle;
        }

        public void SetHit()
        {
            SetEyes();
            LoveBoom();
        }

        private void LoveBoom()
        {
            ps.transform.position = transform.position;
            ps.Play();
        }
        #endregion
    }
}