﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnpolishedGames
{
    public class Fly : MonoBehaviour
    {
        Vector3 previousPosition;
        public Vector3 originalPosition;
        public Transform target { get; protected set; }
        public float speed = 10f;
        public float apex = 2f;

        Vector3 offset;


        System.Action<Fly> onReachTargetCallback;

        // Start is called before the first frame update
        void Start()
        {

        }

        public void SetTarget(GameObject newTarget, Vector3 hitPoint, System.Action<Fly> onReachTargetCallback)
        {
            this.target = newTarget.transform;
            this.offset = hitPoint - target.transform.position;
            originalPosition = transform.position;
            this.onReachTargetCallback = onReachTargetCallback;
        }

        public void SetTarget(GameObject newTarget, System.Action<Fly> onReachTargetCallback)
        {
            SetTarget(newTarget, newTarget.transform.position, onReachTargetCallback);
        }

        // Update is called once per frame
        void Update()
        {
        }

        public IEnumerator FlyTo()
        {
            GetComponent<AudioSource>().Play();

            float t = 0;
            float flyTime = Vector3.Distance(originalPosition, target.position + offset) / speed;
            while (t < flyTime)
            {
                previousPosition = transform.position;
                transform.position = Vector3.Lerp(originalPosition, target.position + offset, t / flyTime);
                transform.position += Vector3.up * apex * Mathf.Sin(Mathf.PI * t / flyTime);

                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(transform.position - previousPosition), 10);

                t += Time.deltaTime;
                yield return null;
            }



            if (onReachTargetCallback != null)
            {
                onReachTargetCallback(this);
            }

            Destroy(gameObject);

        }
    }
}