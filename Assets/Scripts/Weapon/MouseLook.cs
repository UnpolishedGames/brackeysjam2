﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnpolishedGames
{
    public class MouseLook : MonoBehaviour
    {

        public bool captureMousePointer = true;

        public float maxMouseY = 20;
        public float maxMouseX = 15;
        public float mouseSensitivity = 5;

        float y = 0;
        float x = 0;

        // Start is called before the first frame update
        void Start()
        {
            Cursor.lockState = CursorLockMode.Locked;
        }

        // Update is called once per frame
        void Update()
        {
            y += Input.GetAxis("Mouse X") * Time.deltaTime * mouseSensitivity;
            x += -Input.GetAxis("Mouse Y") * Time.deltaTime * mouseSensitivity;

            y += Input.GetAxis("Horizontal") * Time.deltaTime * mouseSensitivity;
            x += Input.GetAxis("Vertical") * Time.deltaTime * mouseSensitivity;

            y = Mathf.Clamp(y, -maxMouseY, maxMouseY);
            x = Mathf.Clamp(x, -maxMouseX, maxMouseX);

            //transform.RotateAround(transform.position, Vector3.up, Input.GetAxis("Mouse X") * Time.deltaTime * mouseSensitivity);

            //transform.RotateAround(transform.position, Vector3.right, -Input.GetAxis("Mouse Y") * Time.deltaTime * mouseSensitivity);

            transform.rotation = Quaternion.Euler(x, y, 0);
        }
    }
}