﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnpolishedGames
{
    public class Shoot : MonoBehaviour
    {
        public LayerMask layerMask;
        public Fly arrowPrefab;

        public SpriteRenderer boundingBox;
        [SerializeField] private float boundingBoxOffset = .5f;

        Fly currentArrow;

        Camera myCamera;

        // Start is called before the first frame update
        void Start()
        {
            myCamera = GetComponent<Camera>();
            boundingBox.enabled = false;
        }

        // Update is called once per frame
        void Update()
        {
            // no arrow, let's create a new one.
            if (currentArrow == null)
            {
                currentArrow = Instantiate(arrowPrefab, transform);
                currentArrow.transform.localPosition += Vector3.forward * 4f;
                currentArrow.transform.Rotate(new Vector3(-25, 0, 0));
            }

            // the currentArrow already has a target, so we shouldn't interact with it.
            if (currentArrow.target != null)
            {
                return;
            }

            RaycastHit hit;
            Ray ray = myCamera.ScreenPointToRay(Input.mousePosition);

            // let the arrow follow the mouse pointer (as to target)
            currentArrow.transform.position = myCamera.ScreenToWorldPoint(Input.mousePosition);
            currentArrow.transform.position = new Vector3(currentArrow.transform.position.x, currentArrow.transform.position.y, transform.position.z) + transform.forward * 4;

            if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask))
            {
                if (hit.collider != null)
                {
                    Debug.DrawLine(transform.position, hit.point, Color.red, 0.25f);
                    boundingBox.enabled = true;
                    boundingBox.transform.position = hit.collider.transform.position + new Vector3(0, boundingBoxOffset, 0);

                    if (Input.GetButtonDown("Fire1"))
                    {
                        currentArrow.transform.SetParent(null);
                        // When target is reached, a callback is called. We can use that to signal GameManager
                        // about which targets to compare.
                        currentArrow.SetTarget(hit.collider.gameObject, hit.collider.bounds.center,
                            (target) =>
                            {
                                CharacterHitEvent characterHitEvent = new CharacterHitEvent();
                                characterHitEvent.character = hit.collider.GetComponent<CharacterBehaviour>();
                                characterHitEvent.FireEvent();
                            });
                        StartCoroutine(currentArrow.FlyTo());
                    }
                }
            }
            else
            {
                boundingBox.enabled = false;
            }

        }
    }
}