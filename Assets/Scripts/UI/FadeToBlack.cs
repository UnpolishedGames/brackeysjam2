﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class FadeToBlack : MonoBehaviour {

    CanvasGroup canvasGroup;

    public bool fadeFromBlackAtStart = false;
     
    // Use this for initialization
    void Start () {
        canvasGroup = GetComponent<CanvasGroup>();

        if (fadeFromBlackAtStart)
        {
            StartCoroutine(FadeBlack(1,0,2f));
        }
	}
	
    public IEnumerator FadeBlack(float fromAlpha, float toAlpha, float delay)
    {
        canvasGroup.blocksRaycasts = true;
        canvasGroup.alpha = fromAlpha;
        float t = 0;
        while (t < delay)
        {
            canvasGroup.alpha = Mathf.Lerp(fromAlpha, toAlpha, t / delay);
            t += Time.deltaTime;
            yield return null;
        }

        canvasGroup.alpha = toAlpha;
        canvasGroup.blocksRaycasts = false;
    } 

	// Update is called once per frame
	void Update () {
		
	}
}
