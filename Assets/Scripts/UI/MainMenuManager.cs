﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuManager : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    public void OnStartGameButtonClicked()
    {
        StartCoroutine(StartGameCR());
    }

    IEnumerator StartGameCR()
    {
        FadeToBlack ftb = GameObject.FindObjectOfType<FadeToBlack>();
        yield return ftb.FadeBlack(0, 1, 1.5f);
        UnityEngine.SceneManagement.SceneManager.LoadScene("Level 1");
    }
}
