﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UnpolishedGames
{
    public class GameController : MonoBehaviour
    {
        #region Variables
        [SerializeField] private List<long> traitValues;

        [SerializeField] private GameObject CharacterPrefab;
        [SerializeField] private TraitAsset[] traitAssets;
        [SerializeField] private int numberOfTraitsPerCharacter;

        [SerializeField] private Transform Despawner;
        [SerializeField] private TextMeshProUGUI score;

        public int couples;
        public int dummies;

        private int characterCount;

        private CharacterBehaviour currentlyHitCharacter;
        #endregion

        #region Unity Methods
        // Use this for initialization
        void Start()
        {
            CharacterHitEvent.RegisterListener(OnCharacterHit);
            PointScoredEvent.RegisterListener(OnPointScoredEvent);
            AllCouplesFoundEvent.RegisterListener(OnAllCouplesFound);
            MatchMadeInHeavenEvent.RegisterListener(OnMatchMadeInHeaven);
            FailedMatchEvent.RegisterListener(OnFailedMatch);

            traitValues = new List<long>();

            characterCount = couples * 2 + dummies;

            SetScorText();

            for (int i = 0; i < traitAssets.Length; i++)
            {
                traitAssets[i].traitValue = (int)Mathf.Pow(2, i);
            }

            List<long> traits = new List<long>();

            for (int i = 0; i < couples; i++)
            {
                TraitAsset[] couple_assets = null;

                while (true)
                {
                    long traitAssetValue = 0;
                    couple_assets = GetUniqueTraits();
                    for (int j = 0; j < couple_assets.Length; j++)
                    {
                        traitAssetValue += couple_assets[j].traitValue;
                    }
                    if (!traitValues.Contains(traitAssetValue))
                    {
                        traitValues.Add(traitAssetValue);
                        break;
                    }
                }

                GameObject char1 = Instantiate(CharacterPrefab);
                char1.GetComponent<CharacterBehaviour>().SetTraitInfo(couple_assets);

                GameObject char2 = Instantiate(CharacterPrefab);
                char2.GetComponent<CharacterBehaviour>().SetTraitInfo(couple_assets);
            }

            for (int i = 0; i < dummies; i++)
            {
                TraitAsset[] dummy_assets = null;

                while (true)
                {
                    long traitAssetValue = 0;
                    dummy_assets = GetUniqueTraits();
                    for (int j = 0; j < dummy_assets.Length; j++)
                    {
                        traitAssetValue += dummy_assets[j].traitValue;
                    }
                    if (!traitValues.Contains(traitAssetValue))
                    {
                        traitValues.Add(traitAssetValue);
                        break;
                    }
                }
                GameObject dummy = Instantiate(CharacterPrefab);
                dummy.GetComponent<CharacterBehaviour>().SetSingle(true);
                dummy.GetComponent<CharacterBehaviour>().SetTraitInfo(dummy_assets);
            }
        }

        private void SetScorText()
        {
            score.text = "Match " + couples + " pairs out of singles left:   " + characterCount.ToString();
        }

        private void SetLevelCompleteText()
        {
            score.text = "Get ready for the next level!";
        }

        private TraitAsset[] GetUniqueTraits()
        {
            int count = 0;
            List<int> traitNumbers = new List<int>();
            TraitAsset[] resultAssets = new TraitAsset[numberOfTraitsPerCharacter];

            List<TraitAsset> traitlistCopy = new List<TraitAsset>();
            for (int i = 0; i < traitAssets.Length; i++)
            {
                traitlistCopy.Add(traitAssets[i]);
            }

            while (count < numberOfTraitsPerCharacter)
            {
                TraitAsset traitAsset = traitlistCopy[(int)UnityEngine.Random.Range(0, (float)traitlistCopy.Count - 1)];
                traitlistCopy.Remove(traitAsset);
                resultAssets[count] = traitAsset;
                count++;
            }
            return resultAssets;
        }

        // Update is called once per frame
        void OnDestroy()
        {
            CharacterHitEvent.UnregisterListener(OnCharacterHit);
            PointScoredEvent.UnregisterListener(OnPointScoredEvent);
            AllCouplesFoundEvent.UnregisterListener(OnAllCouplesFound);
            MatchMadeInHeavenEvent.UnregisterListener(OnMatchMadeInHeaven);
            FailedMatchEvent.UnregisterListener(OnFailedMatch);
        }

        #region Unpolished Methods

        private void OnMatchMadeInHeaven(MatchMadeInHeavenEvent info)
        {
            currentlyHitCharacter = null;
            couples--;
        }

        private void OnFailedMatch(FailedMatchEvent info)
        {
            currentlyHitCharacter = null;
        }
        #endregion

        public void OnPointScoredEvent(PointScoredEvent info)
        {
            characterCount--;
            SetScorText();
            if (characterCount <= dummies)
            {
                AllCouplesFoundEvent allCouplesFoundEvent = new AllCouplesFoundEvent();
                allCouplesFoundEvent.FireEvent();
            }
        }

        private void OnCharacterHit(CharacterHitEvent eventInfo)
        {
            if (eventInfo.character.IsMatched)
            {
                return;
            }

            eventInfo.character.SetHit();

            if (currentlyHitCharacter == null)
            {
                currentlyHitCharacter = eventInfo.character;
            }
            else
            {
                // Check match by comparing trait codes
                if (eventInfo.character != currentlyHitCharacter && eventInfo.character.UniqueTraitsId == currentlyHitCharacter.UniqueTraitsId)
                {
                    //Calculate meetingpoint
                    Vector2 meetingpoint = (eventInfo.character.transform.position + currentlyHitCharacter.transform.position) / 2;
                    eventInfo.character.GetComponent<CharacterAI>().setMeetingPoint(meetingpoint);
                    currentlyHitCharacter.GetComponent<CharacterAI>().setMeetingPoint(meetingpoint);
                }
                else
                {
                    eventInfo.character.ResetEyes();
                    currentlyHitCharacter.ResetEyes();
                    FailedMatchEvent failedMatchEvent = new FailedMatchEvent();
                    failedMatchEvent.FireEvent();
                }
            }
        }

        private void OnAllCouplesFound(AllCouplesFoundEvent info)
        {
            SetLevelCompleteText();
            Invoke("LoadNextScene", 3);
        }

        private void LoadNextScene()
        {
            StartCoroutine(NextScene());
        }

        IEnumerator NextScene()
        {
            FadeToBlack ftb = GameObject.FindObjectOfType<FadeToBlack>();
            yield return ftb.FadeBlack(0, 1, 1.5f);
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
        #endregion
    }
}