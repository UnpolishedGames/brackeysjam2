﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace UnpolishedGames
{
    public class GameReloader : MonoBehaviour
    {

        #region Variables

        #endregion

        #region Unity Methods
        // Use this for initialization
        void Start()
        {
            Invoke("LoadMainMenu", 5);
        }

        private void LoadMainMenu()
        {
            SceneManager.LoadScene(0);
        }
        #endregion

        #region Unpolished Methods

        #endregion
    }
}