﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnpolishedGames
{
    public class SoundManager : MonoBehaviour
    {

        AudioSource audioSource;

        public AudioClip[] clips;

        private void OnEnable()
        {
            CharacterHitEvent.RegisterListener(OnCharacterHit);
            MatchMadeInHeavenEvent.RegisterListener(OnMatchMade);
            FailedMatchEvent.RegisterListener(OnFailedMatch);
            PointScoredEvent.RegisterListener(OnPointScored);
            AllCouplesFoundEvent.RegisterListener(OnAllCouplesFound);
        }

        private void OnDisable()
        {
            CharacterHitEvent.UnregisterListener(OnCharacterHit);
            MatchMadeInHeavenEvent.UnregisterListener(OnMatchMade);
            FailedMatchEvent.UnregisterListener(OnFailedMatch);
            PointScoredEvent.UnregisterListener(OnPointScored);
            AllCouplesFoundEvent.UnregisterListener(OnAllCouplesFound);
        }

        // Start is called before the first frame update
        void Start()
        {
            audioSource = GetComponent<AudioSource>();
        }

        private void OnCharacterHit(CharacterHitEvent eventInfo)
        {
            audioSource.PlayOneShot(clips[0]);
        }

        private void OnMatchMade(MatchMadeInHeavenEvent info)
        {
            audioSource.PlayOneShot(clips[1]);
        }

        private void OnFailedMatch(FailedMatchEvent info)
        {
            audioSource.PlayOneShot(clips[2]);
        }

        private void OnPointScored(PointScoredEvent info)
        {
            audioSource.PlayOneShot(clips[3]);
        }

        private void OnAllCouplesFound(AllCouplesFoundEvent info)
        {
            audioSource.PlayOneShot(clips[4]);
        }
    }

}
