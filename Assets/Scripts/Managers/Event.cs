﻿using System;

namespace UnpolishedGames
{
    public abstract class Event<T> where T : Event<T>
    {
        public string Description;

        private bool hasFired;
        public delegate void EventListener(T info);
        private static event EventListener listeners;

        public static void RegisterListener(EventListener listener)
        {
            listeners += listener;
        }

        public static void UnregisterListener(EventListener listener)
        {
            listeners -= listener;
        }

        public void FireEvent()
        {
            if (hasFired)
            {
                throw new Exception("This event has already fired, to prevent infinite loops you can't refire an event");
            }
            hasFired = true;
            if (listeners != null)
            {
                listeners(this as T);
            }
        }
    }

    #region Class Events
    public class CharacterHitEvent : Event<CharacterHitEvent>
    {
        public CharacterBehaviour character;
    }

    public class MatchMadeInHeavenEvent : Event<MatchMadeInHeavenEvent>
    {
    }

    public class FailedMatchEvent : Event<FailedMatchEvent>
    {
    }

    public class PointScoredEvent : Event<PointScoredEvent>
    {
    }

    public class AllCouplesFoundEvent : Event<AllCouplesFoundEvent>
    {
    }
    #endregion
}